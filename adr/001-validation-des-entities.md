# Validation des entities

* Status: Proposé  
* Date: 2024-03-26


## Contexte

Dans nos Api, nous avons décidé d'utiliser des entities afin d'aggréger les différents entrants nécessaires à l'execution de nos méthodes situées dans la partie Business afin d'éviter de passer un nombre de paramètres trop grand à ces dites méthodes. 
Jusqu'ici nous effectuions tous les contrôles au niveau de la partie business alors que la plupart des controles sont juste des controles d'intégrité des données


## Solutions envisagées :

* Utiliser une méthode validate au niveau des classes entities permettant le contrôle de l'intégrité des données
* Utiliser un système d'annotation javax.validation @Valid afin de valider les propriétés de nos objets


## Décision :

L'option n°1 "Utiliser une méthode validate" a été retenue car celle ci permet de facilement créer des contrôles entre les données de l'objet (Exemple : Vérifier que le numero de personne contenu dans le body de la requete correspond au numéro de personne présent dans le JWT)

Une classe abstraite BaseEntity déclare la méthode validate avec la gestion automatisée de la levée d'erreurs fonctionnelles via la méthode handleValidationException 


### Conséquences positives :

* La validation des entités se fait directement au sein de l'objet => Facilité de compréhension des contrôles effectués
* Le déclenchement de la validation peut se faire directement au niveau du controller (Avant l'appel à la méthode du business)
* Gestion des exceptions automatisée
* Code plus épuré => plus lisible (On comprend beaucoup mieux au niveau business ce qui est fait, on évite la pollution liée aux controles de "surface" )

### Negative Consequences :

* J'en vois pas (Jean Michel Pas Objectif)



## Liens 

