# Gestion des ADR (Architectural Decision Records)

Ce dépôt Git est dédié à la gestion des ADR (Architectural Decision Records) pour enregistrer et documenter les décisions architecturales importantes prises au cours du développement du projet.

## Qu'est-ce qu'un ADR ?

Un ADR (Architectural Decision Record) est un document qui capture une décision architecturale prise lors du développement d'un projet logiciel. Il fournit un moyen de documenter la justification, le contexte, les options considérées et les conséquences de la décision.

## Structure du dépôt

Le dépôt suit une structure simple :

- `adr/` : Ce répertoire contient tous les ADR sous forme de fichiers markdown. Chaque ADR est représenté par un fichier individuel portant un numéro unique.
- `templates/` : Ce répertoire contient des modèles pour la rédaction des ADR.
- `README.md` : Ce fichier README fournit des informations sur la gestion des ADR dans ce dépôt.

## Comment utiliser ce dépôt ?

### Création d'un nouvel ADR

1. Créez une branche et nommez là `NNNN-titre-de-votre-adr`, où `NNNN` est un numéro unique.
2. Copiez le modèle ADR fourni dans `templates/` et renommez-le selon le format `NNNN-titre-de-votre-adr.md`, où `NNNN` est un numéro unique.
3. Remplissez les détails de l'ADR en suivant le modèle.
4. Enregistrez le nouvel ADR dans le répertoire `adr/`.
5. Commitez et poussez votre branche
6. Créez une Merge Request vers main


### Révision d'un ADR existant

1. Pour réviser un ADR existant, modifiez le fichier markdown correspondant dans le répertoire `adr/`.
2. Mettez à jour les détails pertinents de l'ADR, tels que le statut, la justification ou les conséquences.

### Visualisation des ADR

Les ADR peuvent être visualisés directement dans ce dépôt Git en parcourant les fichiers markdown dans le répertoire `adr/`.

## Statuts des ADR

Les ADR peuvent avoir l'un des statuts suivants :

- `Proposé` : Une proposition d'architecture en cours d'évaluation.
- `Rejeté` : Une proposition d'architecture rejetée.
- `Accepté` : Une proposition d'architecture acceptée.
- `Déprécié` : Une proposition d'architecture qui n'est plus recommandée.
- `Remplacé par` : Une proposition d'architecture qui a été remplacée par un autre ADR.

## Exemple d'utilisation

Consultez les ADR existants dans le répertoire `adr/` pour des exemples concrets de décisions architecturales enregistrées.

## Contributions

Les contributions sous forme de nouvelles ADR, de révisions ou d'améliorations de la documentation sont les bienvenues. 

---
