# [Titre de la proposition]

* Status: [Proposé | Rejeté | Accepté | Déprécié | Remplacé par [ADR-0005](0005-example.md)] <!-- optional -->
* Deciders: [Liste des personnes concernées] <!-- optional -->
* Date: [YYYY-MM-DD date de la dernière MAJ] <!-- optional -->


## Contexte

[Description du problème rencontré]


## Options considérées

* [option 1]
* [option 2]
* [option 3]
* … <!-- Le nombre d'options peut varier -->

## Décision

Option: "[option 1]" retenue, car [justification].

### Conséquences positivies  <!-- optional -->

* [Amélioration technique, Amélioration qualité, etc..]
* …

### Conséquences négatives <!-- optional -->

* [Compromet la qualité du code, Ajoute de la complexité, etc..]
* …

## Plus et moins value des options <!-- optional -->

### [option 1]

[Example | description ] <!-- optional -->

* Bien, car [argument a]
* Bien, car [argument b]
* Mauvais, car [argument c]
* … 

### [option 2]

[Example | description ] <!-- optional -->

* Bien, car [argument a]
* Bien, car [argument b]
* Mauvais, car [argument c]
* … 


## Liens <!-- optional -->

* [Type de lien] [Lien vers ADR existante] <!-- exemple: Voir ADR [ADR-0005](0005-example.md) -->
* …